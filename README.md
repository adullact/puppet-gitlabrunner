
# gitlabrunner

This is a puppet module to install and configure Gitlab Runner.

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with gitlabrunner](#setup)
    * [What gitlabrunner affects](#what-gitlabrunner-affects)
    * [Setup requirements](#setup-requirements)
    * [Why set install options to true](#why-set-install-options-to-true)
    * [Beginning with gitlabrunner](#beginning-with-gitlabrunner)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module permit to setup Gitlab Continous Integration by installing and configuring `gitlab-runner`. It is also possible to be helped for installing softwares needed by runners.

## Setup

### What gitlabrunner affects

The module will add somes sources into configuration of package manager.

When you configure an executor `docker`, with helpers enabled, the module can also install a Docker engine.

When you configure an executor `shell`, with helpers enabled, the module can also install a Docker engine, Ruby, Vagrant, Virtualbox and PDK that permits to test Puppet modules.

### Why set install options to true ?

It is possible to set all `install_` options to false :

  * if Docker engine is installed outside of this module.

  * if it is expected to use only `docker` executor with Gitlab Runner.

  * if it is not expected to use Beaker for Puppet acceptance tests.

If it is expected to run Puppet linter, syntax checking and unit tests, it is possible to use `docker` executor with a `ruby` docker image.

If it is expected to use Beaker to run Puppet acceptance tests, `shell` executor is mandatory. And so, all required dependencies for your builds need to be installed on the machine where the Runner is installed. You should consider to set some `install_*` options to `true`.

### Setup Requirements

Requirements are given in `metadata.json`. It gives a list a Puppet modules some are always used, others are used only with parameters `install_*` to `true`.

### Beginning with gitlabrunner

The very basic steps needed for a user to get the module up and running:

```
include gitlabrunner
```

This will install Gitlab Runner software on the node.

## Usage

Example with hiera :

```
---
gitlabrunner::concurrent: 3
gitlabrunner::install_docker: true
gitlabrunner::install_ruby: true

gitlabrunner::runners:
   'runner for puppet acceptance tests':
     authentication-token: 'qsdqd53P_rdt_9Atxnjw'
     url: 'https://gitlabcoordinator.foo'
     executor: 'shell'
     executor-options: 'bash'
   'generic runner':
     authentication-token: 'qsdqx_rdt_9Atxqsddqq'
     url: 'https://gitlabcoordinator.foo'
     executor: 'docker'
     run-untagged: true
     executor-options:
       docker-image: 'alpine:latest'
```

This upper yaml permits to :
  * install `gitlab-runner`.
  * register two differents runners to coordinator.
  * install ruby and docker needed by runners to be fully functionals.
  * configure the runners to be able to run up to 3 jobs in a same time.
  * configure the runner named `generic runner` to accept untagged jobs.

A list of managed options for a runner is given in file `REFERENCES.md`.

Remove a block of runner definition from yaml will unregister the corresponding runner from the gitlab coordinator.

## Limitations

Current compatibles OSes (because tested) are listed in `metadata.json`.

## Development

Home at URL https://gitlab.adullact.net/adullact/puppet-gitlabrunner

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

Details in `CHANGELOG.md`.

```
Copyright (C) 2018 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

