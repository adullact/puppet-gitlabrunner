# Fact : resolver
#
# By default, systemd-resolved uses nameserver 127.0.0.53. systemd-resolved provides
# a local DNS stub listener on IP address 127.0.0.53 on the local loopback interface.
# So inside a container, the ip address is 127.0.0.53 can not be used and DNS resolution
# does not work.
#
# With systemd-resolved the file /etc/resolv.conf can be a link to ../run/systemd/resolve/stub-resolv.conf.
# But systemd-resolved maintains also /usr/lib/systemd/resolv.conf file for compatibility
# with traditional Linux programs. This file may be symlinked from /etc/resolv.conf and
# is always kept up-to-date, containing information about all known DNS servers.

Facter.add(:resolver_path) do
  confine kernel: 'Linux'
  setcode do
    if File.lstat('/etc/resolv.conf').symlink?
      File.readlink('/etc/resolv.conf')
    else
      '/etc/resolv.conf'
    end
  end
end
