Facter.add(:gitlabrunners) do
  confine kernel: 'Linux'
  setcode do
    gitlabrunners = {}
    if File.exist? '/usr/bin/gitlab-runner'
      begin
        # the executed command send output to stderr instead of stdout !!
        runners_list = Facter::Core::Execution.execute("/usr/bin/gitlab-runner --log-format runner list  2>&1 | grep ' Executor'", timeout: 2)
        runners_list.split(%r{\n})[0..-1].map do |runner_line|
          # Output lines are like bellow :
          # runnerdesc       Executor=docker Token=Zb4mSdNbdEsj25sKoyqD URL=https://coordinator.example.org
          runnername, runnerexecutor, runnertocken, runnercoordinatorurl = runner_line.match(%r{^([\w\s\-\_]+)\s+.*\s+Executor.*=(.*)\sToken.*=(.*)\sURL.*=(.*)}).captures
          runnersettings = {}
          runnerdesc = runnername.strip # remove ending space with strip
          runnersettings['executor'] = runnerexecutor
          runnersettings['runner-token'] = runnertocken
          runnersettings['coordinatorurl'] = runnercoordinatorurl
          gitlabrunners[runnerdesc.to_s] = runnersettings
        end
      rescue Facter::Core::Execution::ExecutionFailure
        gitlabrunners['timeout'] = 'command timed out : /usr/bin/gitlab-runner list'
      end
    end
    if !gitlabrunners.nil?
    else
      gitlabrunners = nil
    end
    gitlabrunners
  end
end
