# Changelog

All notable changes to this project will be documented in this file.

## Release 8.0.0

* acceptance : use matrix in gitlab-ci.yml #109
* allow puppetlabs-stdlib < 10.0.0 #108
* use pdk 3.2.0 and Ruby 3.2.0 #107

## Release 7.0.1

* execute docker prune cron ad root #105
* shell helper add some dev libs to ruby compil #104

## Release 7.0.0

* add ubuntu22.04, drop ubuntu18.04  #88
* helper shell use apt instead off archive to install vagrant  #93
* helper shell ci test acceptance do not install/test vagrant and virtualbox  #100
* switch to docker hypervisor with Beaker acceptance tests #101
* configure apt directly with Puppet instead of gitlab install script #102

## Release 6.0.1

* allow puppetlabs-docker < 8.x #96

## Release 6.0.0

* Automatically free space when using runner with Docker engine #3
* With docker helper add ipv6 parameter to enable/disable #91
* Use pdk 3.0.1 drop usage of puppet-module gems #92
* shell helper add paramaters to set versions of installed tools, rename some parameters of docker helper #94

## Release 5.0.0

* drop parameter registration_token, add authentication_token #87

## Release 4.0.0

 * shell helper drop ruby_versions 2.5.1 and 2.5.3 #83
 * move to pdk 2.5.0 #84
 * drop ubuntu-1604 , add ubuntu-20-04 as supported OSes #86

## Release 3.0.0

 * allow puppetlabs-stdlib < 9.x, puppetlabs-apt < 9.x, puppetlabs-docker < 5.x, puppetlabs-vcsrepo < 6.x, puppet-archive < 7.x, puppet-virtualbox < 6.x #81
 * add Puppet7 support, drop puppet 5 #80
 * helper shell does not install pdk #79
 * helper shell allow ruby 2.7.5 #77
 * fix duplicate error about gnupg #78

## Release 2.2.0

 * add ensure parameter to define installed version #75
 * add ruby 2.5.3 in shell helper #73
 * resolver modification should notify docker service #70
 * install of gitlab-runner package should notify service #69

## Release 2.1.0

 * #67 use virtualbox 6.0
 * #66 during acceptance tests split example group apply and idempotency
 * #65 upgrade pdk to release 1.14
 * #64 upgrade vagrant to release 2.2.6
 * #63 custom fact to know resolver

## Release 2.0.0

 * #60 include without parameters gives error
 * #59 update gitlab-runner version to 12.4.1

## Release 1.0.0

 * #50 add contributing guide
 * #51 use Struct with custom datatype Gitlabrunner::Settings
 * #52 allow puppetlabs-stdlib 6.x, puppetlabs-apt 7.x, puppetlabs-docker 3.x, puppetlabs-vcsrepo 3.x, puppet-archive 4.x, puppet-virtualbox 3.x
 * #53 add ubuntu1804 as compatible OS
 * #54 make some resources private
 * #55 lower version for dependence puppetlabs-apt does not install gnupg
 * #56 use docker namespace

## Release 0.7.0

**Features**

  * #44 IPv6 stack is enabled in the kernel but there is no interface that has ::1 address assigned
  * #46 drop puppet 4 add puppet 6

**Bugfixes**

  * #45 with new version of bundler it is possible bundle does not works
  * #41 template with hard coded username in helper for shell runner

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.3

**Features**

  * #35 the module ensure present pdk 1.7.1
  * #34 gitlab-runner needs to run docker with beaker

**Bugfixes**

  * #37 README update production ready
  * #36 the module is managed by pdk 1.7.1

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.2

**Features**

**Bugfixes**

  * #30 simplify `spec_helper_acceptance.rb`
  * #32 add tests about `manage_* => false`
  * #33 extend timeout to exec rbenv install

**Known Issues**

  * #2 Update configuration of an already configured runner.


## Release 0.6.1

**Features**

**Bugfixes**

  * #31 run tests with bundle

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.6.0

**Features**

**Bugfixes**

  * #28 install ruby with helper

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.2

**Features**

**Bugfixes**

  * #23 Enable tests with puppet 4 and puppet 5.
  * #24 Update module to build with pdk 1.7.0.
  * #25 Enable rubocop.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.1

**Features**

  * #22 move to pdk 1.7.0.

**Bugfixes**

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.5.0

**Features**

**Bugfixes**

  * #9  README.md needs to be written.
  * #19 Add custom fact listing all runners and properties.
  * #20 Fix unregister runner.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.4.0

**Features**

**Bugfixes**

  * #7  Fix unit tests (very first step).
  * #16 Fix name of variable `$::gitlabrunner::use_helper`.
  * #17 Use fact of release number instead of codename.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.3.0

**Features**

**Bugfixes**

  * #6 Add `shell` executor.
  * #8 Fix custom type `Gitlabrunner::Settings`.

**Known Issues**

  * #2 Update configuration of an already configured runner.

## Release 0.2.0

**Features**

**Bugfixes**

  * #4 Service resource needs package before.
  * #5 Option concurrent is a global option.

**Known Issues**
