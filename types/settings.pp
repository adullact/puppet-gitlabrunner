#@summary Settings available for a runner
#
type Gitlabrunner::Settings = Struct[{
  'authentication-token' => String[1],
  'url'                  => Stdlib::HTTPSUrl,
  'limit'                => Optional[Integer],
  'executor'             => Enum['docker','shell'],
  'executor-options'     => Optional[Variant[Hash,String]],
}]
