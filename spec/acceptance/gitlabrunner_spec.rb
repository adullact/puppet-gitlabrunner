require 'spec_helper_acceptance'

describe 'gitlabrunner class' do
  context 'with defaults parameters' do
    pp = %(
      include gitlabrunner
    )
    apply2(pp)

    describe package('gitlab-runner') do
      it { is_expected.to be_installed }
    end

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'concurrent = 4' }
    end

    describe service('gitlab-runner') do
      it { is_expected.to be_running }
    end

    describe package('docker-ce') do
      it { is_expected.not_to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.not_to be_installed }
    end
    describe package('libssl-dev') do
      it { is_expected.not_to be_installed }
    end
    describe package('libreadline-dev') do
      it { is_expected.not_to be_installed }
    end
    describe package('zlib1g-dev') do
      it { is_expected.not_to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv') do
      it { is_expected.not_to exist }
    end

    describe command('puppet facts | jq .resolver_path') do
      its(:stdout) { is_expected.to match %r{resolv} }
    end
    describe file('/etc/resolv.conf') do
      its(:content) { is_expected.not_to contain 'nameserver 127.0.0.53' }
    end

    if fact('os.distro.release.major') == '18.04'
      describe file('/etc/resolv.conf') do
        it { is_expected.to be_symlink }
        it { is_expected.to be_linked_to '/run/systemd/resolve/resolv.conf' }
      end
    end
  end

  context 'with executor => docker and helpers to defaults' do
    pp = %(
      class { 'gitlabrunner':
        runners => {
          'puppet-gitlabrunner1-ci' => {
            authentication-token => 'AUTHENTICATION_TOKEN1',
            url                => 'https://gitlab.adullact.net',
            executor           => 'docker',
            executor-options   => {
              docker-image => 'ruby:3.0',
            },
          },
          'puppet-gitlabrunner2-ci' => {
            authentication-token => 'AUTHENTICATION_TOKEN2',
            url                => 'https://gitlab.adullact.net',
            executor           => 'docker',
            executor-options   => {
              docker-image => 'ruby:3.0',
            },
          },
        },
      }
    )
    apply2(pp)

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.to contain 'name = "puppet-gitlabrunner1-ci"' }
      its(:content) { is_expected.to contain 'name = "puppet-gitlabrunner2-ci"' }
    end

    describe command('gitlab-runner list') do
      its(:stderr) { is_expected.to match %r{.*puppet-gitlabrunner1-ci.*} }
      its(:stderr) { is_expected.to match %r{.*puppet-gitlabrunner2-ci.*} }
    end

    describe command('puppet facts | grep -A 2 gitlabrunners') do
      its(:stdout) { is_expected.to match %r{.*gitlabrunners.*} }
    end

    describe package('docker-ce') do
      it { is_expected.not_to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.not_to be_installed }
    end
    describe package('virtualbox-6.0') do
      it { is_expected.not_to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv') do
      it { is_expected.not_to exist }
    end
  end

  context 'without runner defined and some helpers enabled' do
    pp = %(
      class { 'gitlabrunner' :
        dockerhelper_install_docker    => true,
        shellhelper_install_ruby       => true,
        shellhelper_install_vagrant    => true,
      }
    )
    apply2(pp)

    describe file('/etc/gitlab-runner/config.toml') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 600 }
      its(:content) { is_expected.not_to contain 'name = "puppet-gitlabrunner1-ci"' }
      its(:content) { is_expected.not_to contain 'name = "puppet-gitlabrunner2-ci"' }
    end

    describe command('gitlab-runner list') do
      its(:stderr) { is_expected.not_to match %r{.*puppet-gitlabrunner1-ci.*} }
      its(:stderr) { is_expected.not_to match %r{.*puppet-gitlabrunner2-ci.*} }
    end

    describe package('docker-ce') do
      it { is_expected.to be_installed }
    end
    describe package('vagrant') do
      it { is_expected.to be_installed }
    end
    describe user('gitlab-runner') do
      it { is_expected.to belong_to_group 'docker' }
    end
    describe package('libssl-dev') do
      it { is_expected.to be_installed }
    end
    describe package('libreadline-dev') do
      it { is_expected.to be_installed }
    end
    describe package('zlib1g-dev') do
      it { is_expected.to be_installed }
    end
    describe file('/home/gitlab-runner/.rbenv/plugins') do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe file('/home/gitlab-runner/.profile') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
      it { is_expected.to be_mode 644 }
      its(:content) { is_expected.to match %r{PATH=/home/gitlab-runner/\.rbenv/bin:/home/gitlab-runner/\.rbenv/shims:\$PATH} }
    end
    describe file('/home/gitlab-runner/.rbenv/libexec/rbenv') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe command('su - gitlab-runner -c "rbenv whence ruby"') do
      its(:stdout) { is_expected.to match %r{.*2\.7\.5.*} }
    end
    describe file('/home/gitlab-runner/.rbenv/versions/2.7.5/bin/bundle') do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'gitlab-runner' }
      it { is_expected.to be_grouped_into 'gitlab-runner' }
    end
    describe command('su - gitlab-runner -c "RBENV_VERSION=2.7.5 bundle --version"') do
      its(:stdout) { is_expected.to match %r{.*Bundler version.*} }
      its(:exit_status) { is_expected.to be == 0 }
    end
  end
end
