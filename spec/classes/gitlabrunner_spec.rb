require 'spec_helper'

describe 'gitlabrunner' do
  on_supported_os.each do |os, facts|
    context "on #{os} with systemd resolver" do
      let(:facts) do
        facts.merge(
          'gitlabrunners' =>
          {
            'runnerdesc' =>
            {
              'executor'       => 'docker',
              'runner-token'   => 'azerty',
              'coordinatorurl' => 'https://coordinator.foo',
            },
          },
          'resolver_path' => '/run/systemd/resolve/stub-resolv.conf',
        )
      end

      it { is_expected.to compile }

      context 'with dockerhelper_install_docker=true' do
        let(:params) do
          {
            dockerhelper_install_docker: true,
          }
        end

        it { is_expected.to contain_file('/etc/resolv.conf').that_notifies('Service[docker]') }
      end

      context 'with dockerhelper_install_docker=true' do
        let(:params) do
          {
            dockerhelper_install_docker: false,
          }
        end

        it { is_expected.to contain_file('/etc/resolv.conf') }
      end
    end

    context "on #{os} without systemd resolver" do
      let(:facts) do
        facts.merge(
          'gitlabrunners' =>
          {
            'runnerdesc' =>
            {
              'executor'       => 'docker',
              'runner-token'   => 'azerty',
              'coordinatorurl' => 'https://coordinator.foo',
            },
          },
          'resolver_path' => '/etc/onepath',
        )
      end

      it { is_expected.to compile }
      it { is_expected.not_to contain_file('/etc/resolv.conf') }
    end
  end
end
