require 'spec_helper'

gitlab_runner_user = 'gitlab-runner'
rbenv_root = "/home/#{gitlab_runner_user}/.rbenv"

describe 'gitlabrunner::helper::shell' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:pre_condition) do
        needed_objects = <<-EOS
          include gitlabrunner
        EOS
        needed_objects
      end
      let(:facts) do
        facts.merge(
          'resolver_path' => '/etc/onepath',
        )
      end

      context 'with install_vagrant => true' do
        let(:params) do
          {
            install_ruby: false,
            install_vagrant: true,
            install_virtualbox: false,
            runner_account: gitlab_runner_user.to_s,
            vagrant_package_name: 'vagrant',
            vagrant_package_version: '2.2.6',
            vagrant_repository_key_id: '798AEC654E5C15428C8E42EEAA16FCBCA621EFFF',
            vagrant_repository_key_url: 'https://foo.exemple.com',
            virtualbox_package_version: '6.0',
            rbenv_sources: 'https://github.com/rbenv/rbenv.git',
            rbenv_tag: 'v1.2.0',
            rubybuild_sources: 'https://github.com/rbenv/ruby-build.git',
            rubybuild_tag: 'v20211203',
            ruby_versions: ['2.7.5'],
          }
        end

        case facts[:os]['release']['major']
        when '20.04', '22.04'
          it { is_expected.to contain_package('vagrant') }
          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with install_virtualbox => true' do
        let(:params) do
          {
            install_ruby: false,
            install_vagrant: false,
            install_virtualbox: true,
            runner_account: gitlab_runner_user.to_s,
            vagrant_package_name: 'vagrant',
            vagrant_package_version: '2.2.6',
            vagrant_repository_key_id: '798AEC654E5C15428C8E42EEAA16FCBCA621EFFF',
            vagrant_repository_key_url: 'https://foo.exemple.com',
            virtualbox_package_version: '6.0',
            rbenv_sources: 'https://github.com/rbenv/rbenv.git',
            rbenv_tag: 'v1.2.0',
            rubybuild_sources: 'https://github.com/rbenv/ruby-build.git',
            rubybuild_tag: 'v20211203',
            ruby_versions: ['2.7.5'],
          }
        end

        case facts[:os]['release']['major']
        when '20.04', '22.04'
          it { is_expected.to contain_class('virtualbox') }
          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end

      context 'with install_ruby => true' do
        let(:params) do
          {
            install_ruby: true,
            install_vagrant: false,
            install_virtualbox: false,
            runner_account: gitlab_runner_user.to_s,
            vagrant_package_name: 'vagrant',
            vagrant_package_version: '2.2.6',
            vagrant_repository_key_id: '798AEC654E5C15428C8E42EEAA16FCBCA621EFFF',
            vagrant_repository_key_url: 'https://foo.exemple.com',
            virtualbox_package_version: '6.0',
            rbenv_sources: 'https://github.com/rbenv/rbenv.git',
            rbenv_tag: 'v1.2.0',
            rubybuild_sources: 'https://github.com/rbenv/ruby-build.git',
            rubybuild_tag: 'v20211203',
            ruby_versions: ['2.7.5'],
          }
        end

        case facts[:os]['release']['major']
        when '20.04', '22.04'
          it { is_expected.to contain_package('libssl-dev') }
          it { is_expected.to contain_package('libreadline-dev') }
          it { is_expected.to contain_package('zlib1g-dev') }
          it { is_expected.to contain_file("#{rbenv_root}/plugins") }
          it { is_expected.to contain_file("#{rbenv_root}/cache") }
          it { is_expected.to contain_vcsrepo(rbenv_root.to_s) }
          it { is_expected.to contain_vcsrepo("#{rbenv_root}/plugins/ruby-build") }
          it { is_expected.to contain_exec('rbenv install 2.7.5') }

          it { is_expected.to compile }
        else
          it { is_expected.to compile.and_raise(Puppet::Error, %r{unmanaged distro}) }
        end
      end
    end
  end
end
