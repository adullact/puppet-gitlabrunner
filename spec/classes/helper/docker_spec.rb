require 'spec_helper'

describe 'gitlabrunner::helper::docker' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:pre_condition) do
        needed_objects = <<-EOS
          include gitlabrunner
        EOS
        needed_objects
      end
      let(:facts) do
        facts.merge(
          'resolver_path' => '/etc/onepath',
        )
      end

      context 'with install_docker => false' do
        let(:params) do
          {
            install_docker: false,
            runner_account: 'foo',
            enable_docker_usernamespace: false,
            enable_docker_ipv6: false,
            ipv6_cidr: '2001:db8:1::/64',
            prune_weekday: '5',
            prune_hour: '3',
          }
        end

        it { is_expected.not_to contain_class('docker') }
        it { is_expected.to compile }
      end

      context 'with install_docker => true and enable_docker_usernamespace => false ' do
        let(:params) do
          {
            install_docker: true,
            runner_account: 'foo',
            enable_docker_usernamespace: false,
            enable_docker_ipv6: false,
            ipv6_cidr: '2001:db8:1::/64',
            prune_weekday: '5',
            prune_hour: '3',
          }
        end

        it { is_expected.to contain_class('docker').with('extra_parameters' => []) }
        it { is_expected.to compile }
      end

      context 'with install_docker => true and enable_docker_usernamespace => true' do
        let(:params) do
          {
            install_docker: true,
            runner_account: 'foo',
            enable_docker_usernamespace: true,
            enable_docker_ipv6: false,
            ipv6_cidr: '2001:db8:1::/64',
            prune_weekday: '5',
            prune_hour: '3',
          }
        end

        it { is_expected.to contain_class('docker').with('extra_parameters' => ['--userns-remap=default']) }
        it { is_expected.to compile }
      end
    end
  end
end
