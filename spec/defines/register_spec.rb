require 'spec_helper'

describe 'gitlabrunner::register' do
  let(:pre_condition) do
    'include apt
    include gitlabrunner'
  end
  let(:title) { 'namevar' }
  let(:params) do
    {
      authentication_token: 'azerty',
      executor: 'docker',
      coordinator_url: 'https://mygitlab',
      executor_options: { 'docker-image' => 'alpine' },
    }
  end

  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(
          'gitlabrunners' =>
          {
            'runnerdesc' =>
            {
              'executor'       => 'docker',
              'runner-token'   => 'azerty',
              'coordinatorurl' => 'https://coordinator.foo',
            },
          },
          'resolver_path' => '/etc/onepath',
        )
      end

      it { is_expected.to compile }
    end
  end
end
