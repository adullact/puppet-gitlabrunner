require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'
install_module_on(hosts)
install_module_dependencies_on(hosts)

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # needed :
    # * jq to be able to parse json output from facter
    # * bzip2 to extract ruby source code archive
    # * cron from docker helper

    pp = %(
      package { ['jq', 'bzip2', 'cron'] :
        ensure => present,
      }
    )

    apply_manifest(pp, catch_failures: true)
  end
end

# Run it twice and test for idempotency
def apply2(pp)
  it 'applies without error' do
    apply_manifest(pp, catch_failures: true)
  end
  it 'applies idempotently' do
    apply_manifest(pp, catch_changes: true)
  end
end
