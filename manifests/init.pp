#@summary Install and configure gitlan ci runners.
#
#@example
#  class { 'gitlabrunner' :
#    concurrent                  => 4,
#    dockerhelper_install_docker => true,
#    runners                     => {
#      'runner01' => {
#        authentication-token => 'get.secret.from.gitlab.webui',
#        url                  => 'https://mygitlabinstance.foo.org',
#        executor             => 'docker',
#        executor-options     => {
#          docker-image   => 'ruby:3.0',
#          docker-volumes => '/var/run/docker.sock:/var/run/docker.sock',
#        }
#      }
#    }
#  }
#
#@param runners
#  List of runners with their settings.
#  Managed options of runner are :
#    * limit (Integrer)
#    * executor (Enum['docker','shell'])
#    * executor-options (Hash with any executor option, only docker for this module version)
#@param ensure
# Specifies what state the gitlab-runner package should be in.
#@param package_name
# Name of debian package name for gitlab-runner
#@param repository_key_id
# Specifies a GPG key to authenticate Apt package signatures.
#@param repository_key_url
# Specifies the location of an existing GPG key file to install.
#@param concurrent
#  Limits how many jobs globally can be run concurrently. The most upper limit of jobs using all defined runners. 0 does not mean unlimited
#@param check_resolver
#  With hosts running systemd-resolved, it ensure to not use local DNS stub on IP address 127.0.0.53
#@param dockerhelper_install_docker
#  This boolean defined if the docker helper installs Docker CE on runner box.
#@param dockerhelper_enable_usernamespace
#  With true, Docker engine is launched with user name space enable (install_docker have to be true).
#@param dockerhelper_enable_ipv6
#  With true, Enables ipv6 support for the Docker daemon.
#@param dockerhelper_ipv6_cidr
#  With dockerhelper_enable_ipv6 true, IPv6 subnet for fixed IPs in containers.
#@param dockerhelper_prune_weekday
#  The weekday on which to run the docker prune.
#  Day ranges can only be numeric; e.g. '1-5' for weekdays, but not 'Mon-Fri'.
#@param dockerhelper_prune_hour
#  The hour at which to run the docker prune.
#@param shellhelper_install_ruby
#  This boolean define if the shell helper installs Ruby on the runner box (used with shell executor).
#@param shellhelper_install_vagrant
#  This boolean define if the shell helper installs Vagrant on the runner box (used with shell executor).
#@param shellhelper_install_virtualbox
#  This boolean define if the shell helper installs Virtualbox on the runner box (used with shell executor).
#@param shellhelper_vagrant_package_name
#  The Vagrant package name installed by shell helper.
#@param shellhelper_vagrant_package_version
#  The Vagrant package version installed by shell helper.
#@param shellhelper_vagrant_repository_key_id
#  Specifies Hashicorp GPG key to authenticate Apt package signatures
#@param shellhelper_vagrant_repository_key_url
#  Specifies the location of Hashicorp GPG key file to install.
#@param shellhelper_virtualbox_package_version
#  The VirtualBox package version installed by shell helper.
#@param shellhelper_rbenv_sources
#  The URL used to get Rbenv.
#@param shellhelper_rbenv_tag
#  The tag of Rbenv installed by shell helper.
#@param shellhelper_rubybuild_sources
#  The URL used to get Ruby Build
#@param shellhelper_rubybuild_tag
#  The tag of Ruby Build installed by shell helper.
#@param shellhelper_ruby_versions
#  The list of Ruby versions installed by shell helper.
class gitlabrunner (
  Hash[String,Hash] $runners = {},
  Variant[Enum['latest','present'], Pattern['^\d+\.\d+\.\d+$']] $ensure = 'present',
  String[1] $package_name = 'gitlab-runner',
  String $repository_key_id = 'F6403F6544A38863DAA0B6E03F01618A51312F3F',
  Stdlib::HTTPUrl $repository_key_url = 'https://packages.gitlab.com/runner/gitlab-runner/gpgkey',
  Integer $concurrent = 4,
  Boolean $check_resolver = true,
  Boolean $dockerhelper_install_docker = false,
  Boolean $shellhelper_install_ruby = false,
  Boolean $shellhelper_install_vagrant = false,
  Boolean $shellhelper_install_virtualbox = false,
  Boolean $dockerhelper_enable_usernamespace = false,
  Boolean $dockerhelper_enable_ipv6 = true,
  Stdlib::IP::Address::V6::CIDR $dockerhelper_ipv6_cidr = '2001:db8:1::/64',
  String[1] $dockerhelper_prune_weekday = '5',
  String[1] $dockerhelper_prune_hour = '3',
  String[1] $shellhelper_vagrant_package_name = 'vagrant',
  String[1] $shellhelper_vagrant_package_version = '2.4.1-1',
  String[1] $shellhelper_vagrant_repository_key_id = '798AEC654E5C15428C8E42EEAA16FCBCA621E701',
  Stdlib::HTTPUrl $shellhelper_vagrant_repository_key_url = 'https://apt.releases.hashicorp.com/gpg',
  String[1] $shellhelper_virtualbox_package_version = '6.0',
  String[1] $shellhelper_rbenv_sources = 'https://github.com/rbenv/rbenv.git',
  String[1] $shellhelper_rbenv_tag = 'v1.2.0',
  String[1] $shellhelper_rubybuild_sources = 'https://github.com/rbenv/ruby-build.git',
  String[1] $shellhelper_rubybuild_tag = 'v20240517',
  Array[String[1]] $shellhelper_ruby_versions = ['2.7.5'],
) {
  include gitlabrunner::install

  $_runner_account = 'gitlab-runner'

  $_defined_runners_in_facts = $facts['gitlabrunners'] ? {
    undef   => [],
    default => assert_type(Array, keys($facts['gitlabrunners'])) |$expected, $actual| { [] },
  }

  $_defined_runners_in_runners = $runners.map | $_items | { $_items[0] }
  $_defined_runners_in_facts.each | String $_runner_in_facts | {
    if !member($_defined_runners_in_runners , $_runner_in_facts) {
      gitlabrunner::unregister { $_runner_in_facts : }
    }
  }

  if $facts['resolver_path'] =~ 'stub-resolv.conf' and $check_resolver {
    # The node resolver can use nameserver 127.0.0.53. systemd-resolved provides a local DNS stub
    # listener on IP address 127.0.0.53 on the local loopback interface. So, inside containers,
    # the ip address is 127.0.0.53 can not be used and DNS resolution does not work.
    # But systemd-resolved maintains also /run/systemd/resolve/resolv.conf file for compatibility
    # with traditional Linux programs.
    if $dockerhelper_install_docker {
      # When Docker engine starts the resolver configuration is recorded somewhere by docker.
      # So Docker engine have update its knowledge about resolver settings.
      file { '/etc/resolv.conf':
        ensure => link,
        target => '/run/systemd/resolve/resolv.conf',
        notify => Service['docker'],
      }
    } else {
      file { '/etc/resolv.conf':
        ensure => link,
        target => '/run/systemd/resolve/resolv.conf',
      }
    }
  }

  $runners.each | String $_runner_name, Gitlabrunner::Settings $_runner_settings | {
    case $_runner_settings['executor'] {
      'docker','shell': {
        gitlabrunner::register { $_runner_name :
          authentication_token => $_runner_settings['authentication-token'],
          coordinator_url      => $_runner_settings['url'],
          limit                => $_runner_settings['limit'],
          executor             => $_runner_settings['executor'],
          executor_options     => $_runner_settings['executor-options'],
          require              => Package[$package_name],
          notify               => Class['gitlabrunner::config::global'],
        }
      }
      default: {
        fail("executor ${_runner_settings['executor']} is not supported by this module")
      }
    }
  }

  class { 'gitlabrunner::config::global' :
    concurrent => $concurrent,
    require    => Package[$package_name],
  }

  if $dockerhelper_install_docker {
    class { 'gitlabrunner::helper::docker' :
      install_docker              => $dockerhelper_install_docker,
      runner_account              => $_runner_account,
      enable_docker_usernamespace => $dockerhelper_enable_usernamespace,
      enable_docker_ipv6          => $dockerhelper_enable_ipv6,
      ipv6_cidr                   => $dockerhelper_ipv6_cidr,
      prune_weekday               => $dockerhelper_prune_weekday,
      prune_hour                  => $dockerhelper_prune_hour,
    }
  }

  if $shellhelper_install_ruby or $shellhelper_install_vagrant or $shellhelper_install_virtualbox {
    class { 'gitlabrunner::helper::shell' :
      install_ruby               => $shellhelper_install_ruby,
      install_vagrant            => $shellhelper_install_vagrant,
      install_virtualbox         => $shellhelper_install_virtualbox,
      runner_account             => $_runner_account,
      vagrant_package_name       => $shellhelper_vagrant_package_name,
      vagrant_package_version    => $shellhelper_vagrant_package_version,
      vagrant_repository_key_id  => $shellhelper_vagrant_repository_key_id,
      vagrant_repository_key_url => $shellhelper_vagrant_repository_key_url,
      virtualbox_package_version => $shellhelper_virtualbox_package_version,
      rbenv_sources              => $shellhelper_rbenv_sources,
      rbenv_tag                  => $shellhelper_rbenv_tag,
      rubybuild_sources          => $shellhelper_rubybuild_sources,
      rubybuild_tag              => $shellhelper_rubybuild_tag,
      ruby_versions              => $shellhelper_ruby_versions,
    }
  }
}
