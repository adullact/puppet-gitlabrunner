# @summary To unregister a runner from a gitlab-ci coordinator 
#
# @example :
#
#   gitlabrunner::unregister { 'runnername' : } 
#
define gitlabrunner::unregister {
  $_runner_name = assert_type(String, $title)

  exec { "gitlab-runner-unregister-${_runner_name}":
    path    => '/usr/sbin:/usr/bin:/sbin:/bin',
    command => "gitlab-runner unregister --name '${_runner_name}'",
    onlyif  => "gitlab-runner list 2> /dev/stdout | grep -qP '^${_runner_name}\s'",
  }
}
