#
#@summary Register a runner to a gitlab-ci coordinator 
#
#@param authentication_token
#  Runner's authentication token
#@param executor
#  Select executor used by this runner.
#@param coordinator_url
#  Gitlab-ci coordinator url
#@param executor_options
#  List of options to use by the runner with its executor. List of available options : gitlab-runner register --help
#  With executor 'shell', only one option is accepted : 'bash' as String data type.
#@param limit
#   Maximum number of builds processed by this runner.
define gitlabrunner::register (
  String $authentication_token,
  Enum['docker','shell'] $executor,
  Stdlib::HTTPSUrl $coordinator_url,
  Optional[Variant[Hash,String]] $executor_options = undef,
  Integer $limit = 0,
) {
  $_puppet_testing_tag = 'puppet-tests'
  $_runner_name = assert_type(String, $title)

  $_cmd_register_basis = "gitlab-runner register --non-interactive --executor ${executor} --name='${_runner_name}' --url '${coordinator_url}' --token '${authentication_token}' "
  $_cmd_opt_limit = "--limit ${limit}"

  if $executor_options {
    case $executor {
      'shell': {
        $valid_executor_options = assert_type(Enum['bash'], $executor_options)
        $_cmd_opts_executor = "--shell ${valid_executor_options}"
      }
      default: {
        $valid_executor_options = assert_type(Hash[Variant[Pattern['^docker-']], String], $executor_options)
        $_cmd_opts_executor = $valid_executor_options.reduce('') | $memo , $value | { "${memo} --${value[0]} ${value[1]}" }
      }
    }
  } else {
    $_cmd_opts_executor = ''
    # 
    # With docker executor, without executor options will cause the register command failure
    # with a message like 'The docker-image needs to be entered'. 
    # With docker executor, the option docker-image is mandatory. But we want to be able 
    # to know all the running images, only by reading hieradata.
    if $executor == 'docker' {
      fail ('With docker executor, the option docker-image is mandatory.')
    }
  }

  exec { "gitlab-runner-register-${_runner_name}":
    path    => '/usr/sbin:/usr/bin:/sbin:/bin',
    command => "${_cmd_register_basis} ${_cmd_opt_limit} ${_cmd_opts_executor}",
    unless  => "gitlab-runner list 2> /dev/stdout | grep -qP '^${_runner_name}\s'",
  }
}
