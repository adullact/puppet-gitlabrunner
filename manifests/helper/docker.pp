# @summary Install Docker CE
#
#@param install_docker
#  This boolean at true the module install Docker CE on runner box.
#@param runner_account
#  System user account granted to use Docker engine.
#@param enable_docker_usernamespace
#  With true, Docker engine is launched with user name space enable.
#@param enable_docker_ipv6
#  With true, Enables ipv6 support for the docker daemon.
#@param ipv6_cidr
#  With enable_docker_ipv6 true, IPv6 subnet for fixed IPs in containers.
#@param prune_weekday
#  The weekday on which the prune takes place
#@param prune_hour
#  The hour of day on which the prune takes place 
class gitlabrunner::helper::docker (
  Boolean $install_docker,
  String $runner_account,
  Boolean $enable_docker_usernamespace,
  Boolean $enable_docker_ipv6,
  Stdlib::IP::Address::V6::CIDR $ipv6_cidr,
  String $prune_weekday,
  String $prune_hour,
) {
  require gitlabrunner::install
  $_minute = fqdn_rand(59)

  if $install_docker {
    if $enable_docker_usernamespace {
      $extra_parameters = ['--userns-remap=default']
    } else {
      $extra_parameters = []
    }

    # TODO : because docker is a moving product,
    # it is probably better to fix a value to docker version
    class { 'docker':
      ipv6             => $enable_docker_ipv6,
      ipv6_cidr        => $ipv6_cidr,
      docker_users     => [$runner_account],
      extra_parameters => $extra_parameters,
    }

    cron { 'docker-prune':
      command => '/usr/bin/docker system prune --all --force --volumes',
      weekday => $prune_weekday,
      hour    => $prune_hour,
      minute  => $_minute,
    }
  }
}
