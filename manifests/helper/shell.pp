# @summary Install Vagrant, Virtualbox used with shell executor for acceptance tests of Puppet modules.
#
#@param install_ruby
#  This boolean at true the module install Ruby on the runner box (used with shell executor during puppet testing).
#@param install_vagrant
#  This boolean at true the module install Vagrant on the runner box (used with shell executor during puppet testing, hypervisor=vagrant).
#@param install_virtualbox
#  This boolean at true the module install Virtualbox on the runner box (used with shell executor during puppet testing).
#@param runner_account
#  System user account granted to use Docker engine.
#@param vagrant_package_name
#  The Vagrant package name installed by shell helper.
#@param vagrant_package_version
#  The Vagrant package version installed by shell helper.
#@param vagrant_repository_key_id
#  Specifies Hashicorp GPG key to authenticate Apt package signatures
#@param vagrant_repository_key_url
#  Specifies the location of Hashicorp GPG key file to install.
#@param virtualbox_package_version
#  The VirtualBox package version installed by shell helper.
#@param rbenv_sources
#  The URL used to get Rbenv.
#@param rbenv_tag
#  The tag of Rbenv installed by shell helper.
#@param rubybuild_sources
#  The URL used to get Ruby Build
#@param rubybuild_tag
#  The tag of Ruby Build installed by shell helper.
#@param ruby_versions
#  The list of Ruby versions installed by shell helper.
#
class gitlabrunner::helper::shell (
  Boolean $install_ruby,
  Boolean $install_vagrant,
  Boolean $install_virtualbox,
  String $runner_account,
  String[1] $vagrant_package_name,
  String[1] $vagrant_package_version,
  String[1] $vagrant_repository_key_id,
  Stdlib::HTTPUrl $vagrant_repository_key_url,
  String[1] $virtualbox_package_version,
  String[1] $rbenv_sources,
  String[1] $rbenv_tag,
  String[1] $rubybuild_sources,
  String[1] $rubybuild_tag,
  Array[String[1]] $ruby_versions,
) {
  require gitlabrunner::install

  $_rbenv_root = "/home/${runner_account}/.rbenv"

  case $facts['os']['release']['major'] {
    '20.04', '22.04': {
      $_shell_custom_profile = "/home/${runner_account}/.profile"
      $_ruby_requirement = [
        'libyaml-dev',
        'libffi-dev',
        'libssl-dev',
        'libreadline-dev',
        'zlib1g-dev',
        'make',
        'gcc',
      ]
    }
    default: {
      fail("unmanaged os ${facts['os']['distro']['codename']}")
    }
  }

  if $install_vagrant {
    apt::source { 'hashicorp':
      comment  => 'HashiCorp APT repository',
      location => 'http://apt.releases.hashicorp.com',
      release  => $facts['os']['distro']['codename'],
      repos    => 'main',
      pin      => '1001',
      key      => {
        id     => $vagrant_repository_key_id,
        source => $vagrant_repository_key_url,
      },
    }

    package { $vagrant_package_name :
      ensure  => $vagrant_package_version,
      require => Apt::Source['hashicorp'],
    }
  }

  if $install_virtualbox {
    # install virtualbox
    class { 'virtualbox':
      version => $virtualbox_package_version,
    }
  }

  if $install_ruby {
    # install ruby
    package { $_ruby_requirement :
      ensure  => present,
    }

    file { $_shell_custom_profile:
      ensure  => file,
      owner   => $runner_account,
      group   => $runner_account,
      mode    => '0644',
      content => "PATH=/home/${runner_account}/.rbenv/bin:/home/${runner_account}/.rbenv/shims:\$PATH",
    }
    -> vcsrepo { $_rbenv_root :
      ensure   => present,
      provider => 'git',
      owner    => $runner_account,
      group    => $runner_account,
      source   => $rbenv_sources,
      revision => $rbenv_tag,
    }
    -> file { ["${_rbenv_root}/plugins", "${_rbenv_root}/cache"]:
      ensure => directory,
      owner  => $runner_account,
      group  => $runner_account,
      mode   => '0755',
    }
    -> vcsrepo { "${_rbenv_root}/plugins/ruby-build" :
      ensure   => present,
      provider => 'git',
      owner    => $runner_account,
      group    => $runner_account,
      source   => $rubybuild_sources,
      revision => $rubybuild_tag,
    }

    $ruby_versions.each | String $_ruby_version | {
      exec { "rbenv install ${_ruby_version}" :
        user        => $runner_account,
        creates     => "${_rbenv_root}/versions/${_ruby_version}/bin/ruby",
        path        => "${_rbenv_root}/bin:${_rbenv_root}/shims:/bin:/usr/bin",
        cwd         => "/home/${runner_account}",
        timeout     => 600,
        environment => [
          "HOME=/home/${runner_account}",
        ],
        require     => [
          Package[$_ruby_requirement],
          Vcsrepo[$_rbenv_root],
          Vcsrepo["${_rbenv_root}/plugins/ruby-build"],
        ],
      }
      -> exec { "ruby ${_ruby_version} gem install bundler" :
        user        => $runner_account,
        command     => 'gem install bundler',
        creates     => "${_rbenv_root}/versions/${_ruby_version}/bin/bundle",
        path        => "${_rbenv_root}/bin:${_rbenv_root}/shims:/bin:/usr/bin",
        cwd         => "/home/${runner_account}",
        environment => [
          "HOME=/home/${runner_account}",
          "RBENV_VERSION=${_ruby_version}",
        ],
      }
    }
  }
}
