# @summary Install gitlab runner.
#
# @example
#   include gitlabrunner::install
#
# @api private
#
class gitlabrunner::install {
  assert_private()

  require apt

  apt::source { 'gitlabrunner':
    comment  => 'Gitlab APT repository',
    location => 'https://packages.gitlab.com/runner/gitlab-runner/ubuntu/',
    release  => $facts['os']['distro']['codename'],
    repos    => 'main',
    pin      => '1001',
    key      => {
      id     => $gitlabrunner::repository_key_id,
      source => $gitlabrunner::repository_key_url,
    },
  }

  # we use output of command "/usr/bin/gitlab-runner list" used to forge a custom fact.
  # The output of this command can change with versions of gitlab-runner.
  # This can break the parsing of output and so break the custom fact.

  package { $gitlabrunner::package_name :
    ensure  => $gitlabrunner::ensure,
    require => Apt::Source['gitlabrunner'],
    notify  => Service['gitlab-runner'],
  }

  service { 'gitlab-runner' :
    ensure  => running,
    start   => '/usr/bin/gitlab-runner start',
    stop    => '/usr/bin/gitlab-runner stop',
    status  => '/usr/bin/gitlab-runner status',
    require => Package[$gitlabrunner::package_name],
  }
}
